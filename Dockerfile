FROM centos:6.6
MAINTAINER bingo wxdlong@qq.com

#install packages
RUN yum install -y openssh-server openssh-clients

#passwordless ssh
RUN mkdir /var/run/sshd; \
    echo 'root:123456' | chpasswd; \
    sed -i 's/PermitRootLogin without-password/PermitRootLogin yes/' /etc/ssh/sshd_config; \
    ssh-keygen -q -N "" -t dsa -f /etc/ssh/ssh_host_dsa_key; \
    ssh-keygen -q -N "" -t rsa -f /etc/ssh/ssh_host_rsa_key; \
    # SSH login fix. Otherwise user is kicked off after login
    sed 's@session\s*required\s*pam_loginuid.so@session optional pam_loginuid.so@g' -i /etc/pam.d/sshd

RUN mkdir -p /opt/shared;\
    echo "hello shared" > /mnt/abc.log

CMD ["/usr/sbin/sshd", "-D"]